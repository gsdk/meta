<?php

namespace Gsdk\Meta;

class HtmlParser
{
    public function parse(string $html): MetaCollection
    {
        if (empty($html)) {
            return Meta::collect();
        }
        //if (preg_match('/<title>(.*)<\/title>/iU', $html, $m))
        //	$this->setTitle($m[1]);
        //preg_match_all('/<(\w+)(?:\s.*)>/imU', $html, $mTag, PREG_SET_ORDER);
        preg_match_all('/<(\w+\b)(?:([^>]*)\/?>)(?:([^<]*)(?:<\/\w+>))?/im', $html, $mTag, PREG_SET_ORDER);
        if (!$mTag) {
            return Meta::collect();
        }

        $collection = Meta::collect();

        foreach ($mTag as $tag) {
            $attr = [];
            preg_match_all('/(\b(?:\w|-)+\b)\s*=\s*(?:"([^"]*)")/imU', $tag[0], $mAttr, PREG_SET_ORDER);
            if ($mAttr) {
                foreach ($mAttr as $m) {
                    $attr[$m[1]] = $m[2];
                }
            }

            if ($tag[1] === 'meta') {
                $tag = $this->makeTag(Meta::meta(), $attr);
            } elseif ($tag[1] === 'link') {
                $tag = $this->makeTag(Meta::link(), $attr);
//            } elseif ($tag[1] === 'base') {
//                $meta->baseHref($attr['href']);
            } elseif ($tag[1] === 'title' && isset($tag[3])) {
                $tag = Meta::title($tag[3]);
            } else {
                continue;
//                $meta->addContent($tag[0]);
            }

            $collection->add($tag);
        }

        return $collection;
    }

    private function makeTag(TagInterface $tag, array $attributes): TagInterface
    {
        foreach ($attributes as $k => $v) {
            $tag->$k($v);
        }

        return $tag;
    }
}
