<?php

namespace Gsdk\Meta\Support;

abstract class AbstractTag
{
    protected array $attributes = [];

    protected array $properties = [];

    abstract protected function tag(): string;

    public function __get($name)
    {
        return $this->getAttribute($name);
    }

    public function getAttribute(string $name): ?string
    {
        return $this->attributes[$name] ?? null;
    }

    public function hasProperty(string $name): bool
    {
        return in_array($name, $this->properties);
    }

    protected function setAttribute(string $name, string $value): static
    {
        $this->attributes[$name] = $value;

        return $this;
    }

    protected function setProperty(string $name, bool $flag): static
    {
        if ($flag) {
            $this->properties[] = $name;
        }

        return $this;
    }

    public function toHtml(): string
    {
        $s = '<' . $this->tag();
        foreach ($this->attributes as $k => $v) {
            if ($v) {
                $s .= " $k=\"$v\"";
            }
        }
        foreach ($this->properties as $name) {
            $s .= ' ' . $name;
        }
        $s .= '>';

        return $s;
    }

    public function __toString(): string
    {
        return $this->toHtml();
    }
}
