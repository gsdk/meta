<?php

namespace Gsdk\Meta\Support;

trait HasCorsTrait
{
    public function crossorigin(string $crossorigin): static
    {
        return $this->setAttribute(__FUNCTION__, $crossorigin);
    }

    public function referrerpolicy(string $referrerpolicy): static
    {
        return $this->setAttribute(__FUNCTION__, $referrerpolicy);
    }
}