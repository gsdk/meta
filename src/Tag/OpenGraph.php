<?php

namespace Gsdk\Meta\Tag;

use Gsdk\Meta\Support\AbstractTag;
use Gsdk\Meta\TagInterface;

class OpenGraph extends AbstractTag implements TagInterface
{
    private const UNIQUE_PROPERTIES = [
        'og:title',
        'og:description',
        'og:keywords',
        'og:url',
        'og:type',
        'og:image',
    ];

    public function __construct(string $property)
    {
        if (!str_starts_with($property, 'og:')) {
            $property = 'og:' . $property;
        }

        return $this->setAttribute('property', $property);
    }

    public function content(string $content): static
    {
        return $this->setAttribute(__FUNCTION__, $content);
    }

    protected function tag(): string
    {
        return 'meta';
    }

    public function uniqueKey(): ?string
    {
        if ($this->isUniqueProperty()) {
            return $this->getPropertyUniqueKey();
        } else {
            return null;
        }
    }

    private function isUniqueProperty(): bool
    {
        return isset($this->attributes['property'])
            && in_array(strtolower($this->attributes['property']), self::UNIQUE_PROPERTIES);
    }

    private function getPropertyUniqueKey(): string
    {
        return 'meta-property-' . strtolower($this->attributes['property']);
    }
}
