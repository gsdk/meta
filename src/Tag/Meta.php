<?php

namespace Gsdk\Meta\Tag;

use Gsdk\Meta\Support\AbstractTag;
use Gsdk\Meta\TagInterface;

class Meta extends AbstractTag implements TagInterface
{
    private const UNIQUE_NAMES = [
        'title',
        'description',
        'keywords',
        'viewport',
        'robots',
        'author',
    ];

    private const UNIQUE_HTTP_EQUIV = ['content-type', 'x-ua-compatible', 'content-language'];

    public function name(string $name): static
    {
        return $this->setAttribute(__FUNCTION__, $name);
    }

    public function httpEquiv(string $httpEquiv): static
    {
        return $this->setAttribute('http-equiv', $httpEquiv);
    }

    public function content(string $content): static
    {
        return $this->setAttribute(__FUNCTION__, $content);
    }

    public function charset(string $charset): static
    {
        return $this->setAttribute(__FUNCTION__, $charset);
    }

    protected function tag(): string
    {
        return 'meta';
    }

    public function uniqueKey(): ?string
    {
        if (isset($this->attributes['charset'])) {
            return 'meta-charset';
        } elseif ($this->isUniqueName()) {
            return $this->getNameUniqueKey();
        } elseif ($this->isUniqueHttpEquiv()) {
            return $this->getHttpEquivUniqueKey();
        } else {
            return null;
        }
    }

    private function isUniqueName(): bool
    {
        return isset($this->attributes['name'])
            && in_array(strtolower($this->attributes['name']), self::UNIQUE_NAMES);
    }

    private function isUniqueHttpEquiv(): bool
    {
        return isset($this->attributes['http-equiv'])
            && in_array(strtolower($this->attributes['http-equiv']), self::UNIQUE_HTTP_EQUIV);
    }

    private function getNameUniqueKey(): string
    {
        return 'meta-name-' . strtolower($this->attributes['name']);
    }

    private function getHttpEquivUniqueKey(): string
    {
        return 'meta-http-equiv-' . strtolower($this->attributes['http-equiv']);
    }
}
