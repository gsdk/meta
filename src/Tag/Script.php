<?php

namespace Gsdk\Meta\Tag;

use Gsdk\Meta\Support\AbstractTag;
use Gsdk\Meta\Support\HasCorsTrait;
use Gsdk\Meta\TagInterface;

class Script extends AbstractTag implements TagInterface
{
    use HasCorsTrait;

    protected function tag(): string
    {
        return 'script';
    }

    public function src(string $src): static
    {
        return $this->setAttribute(__FUNCTION__, $src);
    }

    public function type(string $type): static
    {
        return $this->setAttribute(__FUNCTION__, $type);
    }

    public function async(bool $flag = true): static
    {
        return $this->setProperty(__FUNCTION__, $flag);
    }

    public function defer(bool $flag = true): static
    {
        return $this->setProperty(__FUNCTION__, $flag);
    }

    public function uniqueKey(): ?string
    {
        return null;
    }
}
