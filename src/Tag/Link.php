<?php

namespace Gsdk\Meta\Tag;

use Gsdk\Meta\Support\AbstractTag;
use Gsdk\Meta\Support\HasCorsTrait;
use Gsdk\Meta\TagInterface;

class Link extends AbstractTag implements TagInterface
{
    use HasCorsTrait;

    private const UNIQUE_REL = [
        'canonical'
    ];

    public function as(string $as): static
    {
        return $this->setAttribute(__FUNCTION__, $as);
    }

    public function rel(string $rel): static
    {
        return $this->setAttribute(__FUNCTION__, $rel);
    }

    public function type(string $type): static
    {
        return $this->setAttribute(__FUNCTION__, $type);
    }

    public function title(string $title): static
    {
        return $this->setAttribute(__FUNCTION__, $title);
    }

    public function href(string $href): static
    {
        return $this->setAttribute(__FUNCTION__, $href);
    }

    public function hreflang(string $hreflang): static
    {
        return $this->setAttribute(__FUNCTION__, $hreflang);
    }

    public function charset(string $charset): static
    {
        return $this->setAttribute(__FUNCTION__, $charset);
    }

    public function media(string $media): static
    {
        return $this->setAttribute(__FUNCTION__, $media);
    }

    public function uniqueKey(): ?string
    {
        if ($this->isUniqueRel()) {
            return $this->getRelUniqueKey();
        } else {
            return null;
        }
    }

    private function isUniqueRel(): bool
    {
        return isset($this->attributes['rel'])
            && in_array(strtolower($this->attributes['rel']), self::UNIQUE_REL);
    }

    private function getRelUniqueKey(): string
    {
        return 'link-rel-' . strtolower($this->attributes['rel']);
    }

    protected function tag(): string
    {
        return 'link';
    }
}
