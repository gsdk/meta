<?php

namespace Gsdk\Meta\Tag;

use Gsdk\Meta\TagInterface;

class Title implements TagInterface
{
    protected string $title;

    public function __construct(string $title)
    {
        $this->title = $title;
    }

    public function uniqueKey(): string
    {
        return 'title';
    }

    public function toHtml(): string
    {
        return "<title>$this->title</title>";
    }

    public function __toString(): string
    {
        return $this->toHtml();
    }
}
