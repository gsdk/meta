<?php

namespace Gsdk\Meta;

interface TagInterface
{
    public function uniqueKey(): ?string;

    public function toHtml(): string;

    public function __toString(): string;
}
