<?php

namespace Gsdk\Meta;

use Gsdk\Meta\Tag\Link;
use Gsdk\Meta\Tag\Script;

class MetaCollection
{
    /**
     * @var TagInterface[]
     */
    protected array $items = [];

    public function icon(string $href): static
    {
        return $this->add(Meta::linkRel('icon')->href($href));
    }

    public function charset(string $charset): static
    {
        return $this->add(Meta::meta()->charset($charset));
    }

    public function title(string $title): static
    {
        return $this->add(Meta::title($title));
    }

    public function description(string $description): static
    {
        return $this->add(Meta::metaName('description', $description));
    }

    public function keywords(string $keywords): static
    {
        return $this->add(Meta::metaName('keywords', $keywords));
    }

    public function viewport(string $content): static
    {
        return $this->add(Meta::metaName('viewport', $content));
    }

    public function author(string $author): static
    {
        return $this->add(Meta::metaName('author', $author));
    }

    public function robots(string $robots): static
    {
        return $this->add(Meta::metaName('robots', $robots));
    }

    public function contentType(string $content): static
    {
        return $this->add(Meta::metaHttpEquiv('Content-Type', $content));
    }

    public function contentLanguage(string $language): static
    {
        return $this->add(Meta::metaHttpEquiv('Content-language', $language));
    }

    public function xUACompatible(string $content): static
    {
        return $this->add(Meta::metaHttpEquiv('X-UA-Compatible', $content));
    }

    public function ogTitle(string $title): static
    {
        return $this->add(Meta::og('og:title')->content($title));
    }

    public function ogDescription(string $description): static
    {
        return $this->add(Meta::og('og:description')->content($description));
    }

    public function ogKeywords(string $keywords): static
    {
        return $this->add(Meta::og('og:keywords')->content($keywords));
    }

    public function addScript(Script|string $script): static
    {
        return $this->add(is_string($script) ? Meta::script($script) : $script);
    }

    public function addStyle(Link|string $style): static
    {
        return $this->add(is_string($style) ? Meta::stylesheet($style) : $style);
    }

    public function add(TagInterface $tag): static
    {
        $uniqueKey = $tag->uniqueKey();
        if ($uniqueKey) {
            $this->items[$uniqueKey] = $tag;
        } else {
            $this->items[] = $tag;
        }

        return $this;
    }

    public function isEmpty(): bool
    {
        return empty($this->items);
    }

    /**
     * @return TagInterface[]
     */
    public function all(): array
    {
        return array_values($this->items);
    }

    public function toHtml(): string
    {
        return implode("\n", array_map(fn(TagInterface $tag) => $tag->toHtml(), $this->items));
    }

    public function __toString(): string
    {
        return $this->toHtml();
    }
}