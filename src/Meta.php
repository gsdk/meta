<?php

namespace Gsdk\Meta;

use Gsdk\Meta\Tag\Link;
use Gsdk\Meta\Tag\Meta as MetaTag;
use Gsdk\Meta\Tag\OpenGraph;
use Gsdk\Meta\Tag\Script;
use Gsdk\Meta\Tag\Title;

class Meta
{
    public static function collect(): MetaCollection
    {
        return new MetaCollection();
    }

    public static function link(): Link
    {
        return new Link();
    }

    public static function meta(): MetaTag
    {
        return new MetaTag();
    }

    public static function og(string $property): OpenGraph
    {
        return new OpenGraph($property);
    }

    public static function script(string $src = null): Script
    {
        $script = new Script();
        if ($src) {
            $script->src($src);
        }

        return $script;
    }

    public static function stylesheet(string $href): Link
    {
        return self::link()
            ->rel('stylesheet')
            ->type('text/css')
            ->media('screen')
            ->href($href);
    }

    public static function linkRel(string $rel): Link
    {
        return self::link()->rel($rel);
    }

    public static function metaName(string $name, string $content = null): MetaTag
    {
        $meta = self::meta()->name($name);

        if ($content) {
            $meta->content($content);
        }

        return $meta;
    }

    public static function metaHttpEquiv(string $httpEquiv, string $content = null): MetaTag
    {
        $meta = self::meta()->httpEquiv($httpEquiv);

        if ($content) {
            $meta->content($content);
        }

        return $meta;
    }

    public static function title(string $title): Title
    {
        return new Title($title);
    }

    public static function fromString(string $html): MetaCollection
    {
        return (new HtmlParser())->parse($html);
    }
}
